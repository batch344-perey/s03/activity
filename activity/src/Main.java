import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        System.out.println("Input an integer whose factorial will be computed:");

        Scanner in = new Scanner(System.in);
        int num = 0;

        try{
            num = in.nextInt();
        }
        catch (Exception e) {
            System.out.println("Invalid input");
        }


        int answer = 1;
        int counter = 1;

        //for WHILE LOOP
        if(num > 0) {
            while (counter <= num){
                answer = answer *= counter;
                counter ++;
            }

            System.out.println("The factorial of " + num + " is " + answer);
        } else {
            System.out.println("The value cannot be computed.");
        }

        //for FOR LOOP
        answer = 1;
        counter = 1;

        if(num > 0) {
            for(counter = 1; counter <= num; counter ++){
                answer = answer *= counter;
            }
            System.out.println("The factorial of " + num + " is " + answer);
        } else {
            System.out.println("The value cannot be computed.");
        }

    }
}